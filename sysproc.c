#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "spinlock.h"
#include "mamadlock.h"

/// Here2
// alarm
int sys_alarm(void)
{
  int ticks;
  void (*handler)();

  if (argint(0, &ticks) < 0)
  {
    cprintf("Input is not valid\n");
    return -1;
  }
  if (argptr(1, (char **)&handler, 1))
    return -1;
  // myproc()->alarmticks = ticks;
  // myproc()->alarmleft = ticks; // as init
  // myproc()->alarmhandler = handler;
  return 0;
}

// sys_printsyscalls
int sys_printsyscalls(void)
{
  return printsyscalls();
}

// sys_set_edx
int sys_set_edx(int value)
{
  argint(0, &value);
  myproc()->tf->edx = (uint)value;
  return 0;
}

// Here 4
struct mamadlock mlk;
int sys_test_mamadlock(void)
{
  acquiremamad(&mlk);
  for (int i = 0; i < 500000; i++)
  {
    sys_set_edx(10);
  }
  cprintf("DONE!! pid:%d\n", myproc()->pid);
  releasemamad(&mlk);
  exit();
  return 0;
}

int sys_syscalls_num(void)
{
  cprintf("Total syscall counter:%d\n", syscallcounter);
  pushcli();
  for (int i = 0; i < NCPU; i++)
  {
    cprintf("Core %d syscall counter:%d\n", i, cpus[i].syscallcounter);
  }
  popcli();
  return 0;
}

int sys_read_registers(void)
{
  cprintf("edi: %d\n", myproc()->tf->edi);
  cprintf("esi: %d\n", myproc()->tf->esi);
  cprintf("ebp: %d\n", myproc()->tf->ebp);
  cprintf("oesp: %d\n", myproc()->tf->oesp);
  cprintf("ebx: %d\n", myproc()->tf->ebx);
  cprintf("edx: %d\n", myproc()->tf->edx);
  cprintf("ecx: %d\n", myproc()->tf->ecx);
  cprintf("eax: %d\n", myproc()->tf->eax);
  return 0;
}

int sys_count_num_of_digits(int num)
{
  argint(0, &num);
  int ans = 0;
  while (num != 0)
  {
    num /= 10;
    ans++;
  }
  cprintf("%d\n", ans);
  return 0;
}

int sys_fork(void)
{
  return fork();
}

int sys_exit(void)
{
  exit();
  return 0; // not reached
}

int sys_wait(void)
{
  return wait();
}

int sys_kill(void)
{
  int pid;

  if (argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int sys_getpid(void)
{
  return myproc()->pid;
}

int sys_sbrk(void)
{
  int addr;
  int n;

  if (argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if (growproc(n) < 0)
    return -1;
  return addr;
}

int sys_sleep(void)
{
  int n;
  uint ticks0;

  if (argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while (ticks - ticks0 < n)
  {
    if (myproc()->killed)
    {
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

// Here 3
int sys_changequeue(void)
{
  int pid, queue;
  if (argint(0, &pid) < 0 || argint(1, &queue) < 0)
    return -1;
  return changequeue(pid, queue);
}

int sys_setticket(void)
{
  int pid, numOfTickets;
  if (argint(0, &pid) < 0)
    return -1;
  if (argint(1, &numOfTickets) < 0)
    return -1;
  return setticket(pid, numOfTickets);
}

int sys_printprocesses(void)
{
  return printprocesses();
}