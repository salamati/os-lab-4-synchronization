// HERE 4
#include "types.h"
#include "defs.h"
#include "param.h"
#include "x86.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "spinlock.h"
#include "mamadlock.h"

void addToQueue(struct proc *p, struct mamadlock *lk)
{
  for (int i = 0; i < NPROC; i++)
  {
    if (lk->queue[i] != 0 && lk->queue[i]->pid == p->pid)
    {
      return;
    }
  }
  lk->queue[lk->lastIndex] = p;
  lk->lastIndex = (lk->lastIndex + 1) % NPROC;
  lk->queueNum++;
}

void removeFromQueue(struct proc *p, struct mamadlock *lk)
{
  for (int i = 0; i < NPROC; i++)
  {
    if (lk->queue[i] != 0 && lk->queue[i]->pid == p->pid)
    {
      lk->queue[i] = 0;
      lk->queueNum--;
      return;
    }
  }
}

void findChosenProc(struct mamadlock *lk)
{
  int minPid = 10e6;
  for (int i = 0; i < NPROC; i++)
  {
    if (lk->queue[i] != 0 && lk->queue[i]->pid < minPid)
    {
      minPid = lk->queue[i]->pid;
    }
  }
  lk->chosenProcPid = minPid;
}

void printQueueAndHolder(struct mamadlock *lk)
{
  cprintf("---------- mamadlock monitor ----------\n");
  cprintf("Holder:%d\n", lk->pid);
  cprintf("Queue: \n");
  for (int i = 0; i < NPROC; i++)
  {
    if (lk->queue[i] != 0)
    {
      cprintf("%d ", lk->queue[i]->pid);
    }
  }
  cprintf("\n---------------------------------------\n");
}

void initmamadlock(struct mamadlock *lk, char *name)
{
  initlock(&lk->lk, "mamad lock");
  lk->name = name;
  lk->locked = 0;
  lk->pid = 0;
  lk->lastIndex = 0;
  lk->chosenProcPid = -1;
  lk->queueNum = 0;
}

void acquiremamad(struct mamadlock *lk)
{
  acquire(&lk->lk);
  while (lk->locked || (lk->queueNum > 0 && lk->chosenProcPid != myproc()->pid))
  {
    addToQueue(myproc(), lk);
    sleep(lk, &lk->lk);
  }
  removeFromQueue(myproc(), lk);
  lk->locked = 1;
  lk->pid = myproc()->pid;
  printQueueAndHolder(lk);
  release(&lk->lk);
}

void releasemamad(struct mamadlock *lk)
{
  acquire(&lk->lk);
  lk->locked = 0;
  lk->pid = 0;
  findChosenProc(lk);
  wakeup(lk);
  release(&lk->lk);
}

int holdingmamad(struct mamadlock *lk)
{
  int r;
  acquire(&lk->lk);
  r = lk->locked && (lk->pid == myproc()->pid);
  release(&lk->lk);
  return r;
}
